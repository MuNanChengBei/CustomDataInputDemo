package study.lzl.customdatainputdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 自定义数字输入键盘
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText edt;
    private Button mButton;

    private TextView tvDeposit_1;
    private TextView tvDeposit_2;
    private TextView tvDeposit_3;
    private TextView tvDeposit_4;
    private TextView tvDeposit_5;
    private TextView tvDeposit_6;
    private TextView tvDeposit_7;
    private TextView tvDeposit_8;
    private TextView tvDeposit_9;
    private TextView tvDeposit_0;
    private TextView tvDeposit_point;
    private TextView tvDeposit_del;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initListener();
    }

    private void initListener() {
        tvDeposit_1.setOnClickListener(this);
        tvDeposit_2.setOnClickListener(this);
        tvDeposit_3.setOnClickListener(this);
        tvDeposit_4.setOnClickListener(this);
        tvDeposit_5.setOnClickListener(this);
        tvDeposit_6.setOnClickListener(this);
        tvDeposit_7.setOnClickListener(this);
        tvDeposit_8.setOnClickListener(this);
        tvDeposit_9.setOnClickListener(this);
        tvDeposit_0.setOnClickListener(this);
        tvDeposit_point.setOnClickListener(this);
        tvDeposit_del.setOnClickListener(this);


        mButton.setOnClickListener(this);
        edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edt.getText().toString().length()!=0){
                    mButton.setClickable(true);
                    mButton.setSelected(true);
                }else {
                    mButton.setSelected(false);
                    mButton.setClickable(false);
                }
                edt.setSelection(edt.length());//将光标定位到最后一位

            }
        });

    }

    private void initView() {
        tvDeposit_1=findViewById(R.id.deposit_1);
        tvDeposit_2=findViewById(R.id.deposit_2);
        tvDeposit_3=findViewById(R.id.deposit_3);
        tvDeposit_4=findViewById(R.id.deposit_4);
        tvDeposit_5=findViewById(R.id.deposit_5);
        tvDeposit_6=findViewById(R.id.deposit_6);
        tvDeposit_7=findViewById(R.id.deposit_7);
        tvDeposit_8=findViewById(R.id.deposit_8);
        tvDeposit_9=findViewById(R.id.deposit_9);
        tvDeposit_0=findViewById(R.id.deposit_zero);
        tvDeposit_point=findViewById(R.id.deposit_point);
        tvDeposit_del=findViewById(R.id.deposit_del);
        edt=findViewById(R.id.editext);
        mButton= findViewById(R.id.button);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.deposit_1:
                inputNum("1");
                break;
            case R.id.deposit_2:
                inputNum("2");
                break;
            case R.id.deposit_3:
                inputNum("3");
                break;
            case R.id.deposit_4:
                inputNum("4");
                break;
            case R.id.deposit_5:
                inputNum("5");
                break;
            case R.id.deposit_6:
                inputNum("6");
                break;
            case R.id.deposit_7:
                inputNum("7");
                break;
            case R.id.deposit_8:
                inputNum("8");
                break;
            case R.id.deposit_9:
                inputNum("9");
                break;
            case R.id.deposit_zero:
                inputNum("0");
                break;
            case R.id.deposit_point:
                inputNum(".");
                break;
            case R.id.deposit_del:
                delectOrClean();
                break;

            case R.id.button:
                Toast.makeText(getApplicationContext(),"当前数字:"+edt.getText().toString(),Toast.LENGTH_SHORT).show();
                break;


        }
    }

    private void delectOrClean() {
        if (edt.getText().toString().length()==0){
            edt.setText("");
        }else {
            edt.setText(edt.getText().toString().substring(0,edt.getText().length()-1));
        }
    }

    /**
     *录入输入的数字
     */
    private void inputNum(String inputNum) {
        //判断输入的是什么
        if (TextUtils.equals(inputNum,"0")){//如果输入的是0
            if (edt.getText().length()==1){
                if (edt.getText().toString().indexOf("0")!=0){//判断edt中首位是否是0
                    edt.append(inputNum);
                }
            }else if (edt.getText().length()==0||edt.getText().length()>=2){
                edt.append(inputNum);
            }
        }else if (TextUtils.equals(inputNum,".")){//如果输入的是小数点
            if (edt.getText().length()==0){
                edt.setText("0.");
            }else if (!edt.getText().toString().contains(".")){//数字中没有包含小数点
                edt.append(".");
            }
        }else {//输入的是1-9的正整数
            edt.append(inputNum);
        }
    }
}
